import { test, expect } from '@playwright/test';

const baseUrl =  "https://bigcitypet.qa.covetruspharmacy.io/";

//Externalize page URLs in next iteration
//To update baseline image set execute: npx playwright test --update-snapshots
//Max Pixel Difference set to 50, confirm with Kayla in future

//Screenshot compare of Address Book
test('UI: Address Book', async ({ page }) => {
  await page.goto(baseUrl+'/covetrus/my-account/address-book');
  await expect(page).toHaveScreenshot({ maxDiffPixels: 50 });

  });

  test('UI Image Comparison: Profile Page', async ({ page }) => {

    await page.goto(baseUrl+'/covetrus/my-account/profile');
    await expect(page).toHaveScreenshot({ maxDiffPixels: 50 });
    });

    test('UI Image Comparison: My Pets Page', async ({ page }) => {

      await page.goto(baseUrl+'/covetrus/my-account/my-pets');
      await expect(page).toHaveScreenshot({ maxDiffPixels: 50 });
      });

      test('UI Image Comparison: Wallet Page', async ({ page }) => {

        await page.goto(baseUrl+'/covetrus/my-account/wallet');
        await expect(page).toHaveScreenshot({ maxDiffPixels: 50 });
        });

        test('UI Image Comparison: Cart Page', async ({ page }) => {

          await page.goto(baseUrl+'/covetrus/cart');
          await expect(page).toHaveScreenshot({ maxDiffPixels: 50 });
          });

          test('UI Image Comparison: Heartgard product page', async ({ page }) => {

            await page.goto(baseUrl+'/covetrus/product/1992988SD/Heartgard®%20Plus%20Dog%20Chews%20Brown%2051-100%20lbs%2012pk');
            await expect(page).toHaveScreenshot({ maxDiffPixels: 50 });
            });

            test('UI Image Comparison: Wishlist page', async ({ page }) => {

              await page.goto(baseUrl+'/covetrus/my-account/wishlist');
              await expect(page).toHaveScreenshot({ maxDiffPixels: 50 });
              });

              test('UI Image Comparison: Orders page', async ({ page }) => {

                await page.goto(baseUrl+'/covetrus/my-account/orders');
                await expect(page).toHaveScreenshot({ maxDiffPixels: 50 });
                });
  