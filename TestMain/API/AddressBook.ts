import { test, expect } from "@playwright/test";
//externalize for paramater and environment switching next iteration
const baseUrl = "https://bigcitypet.qa.covetruspharmacy.io/";




test("Create Address",async ({ request }) => {
    
    const createResponse = await request.post(baseUrl+`covetrus/my-account/address-book`, {
        headers : {
            "Authorization" : "Bearer "
        },
        data : {
            "FirstName": `Automation First`,
            "LastName": "Automation",
            "Address": "Automation Drive"
          
        }
    })
}
)


test("AddressBook duplicate",async ({ request }) => {

    const createDuplicateNoteResponse = await request.post(baseUrl+`covetrus/my-account/address-book`, {
        data : {
            "FirstName": `Automation First`,
            "LastName": `Automation`,
          
        }
    })

    let duplicateResponseJson = await createDuplicateNoteResponse.json();
    let responseErrorCode = duplicateResponseJson['code'];
    expect(responseErrorCode).toContain("Address already exists");
})

test("Update Address",async ({ request }) => {
    const patchResponse = await request.post(baseUrl+`covetrus/my-account/address-book`, {

        data : {
            "FirstName": `Automation First2 `,
            "LastName": "Automation2",
            "PhoneNumber": "4802615275",
            "StreetAddress": `1207 E. 8th Street`
    
        }
    })

    expect(patchResponse.ok())
})
//Address Count should equal 2
test("Address Count Validation",async ({ request }) => {
    const patchResponse = await request.patch(baseUrl+`covetrus/my-account/address-book`, {

    })
    let AddressCount = await patchResponse.json();
    AddressCount.toEqual('2');
    expect(patchResponse.ok())
})

test("Address Deleted",async ({ request }) => {
    const getResponse = await request.get(baseUrl+`covetrus/my-account/address-book`, {
       
    })

    let getResponseJson = await getResponse.json();

    let isDeleted = await getResponseJson['isDeleted'];


    expect(isDeleted).toEqual(true);

})