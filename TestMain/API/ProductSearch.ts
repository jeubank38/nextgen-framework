import { expect, test } from "@playwright/test";

//externalize for paramater and environment switching next iteration
const baseUrl = "https://bigcitypet.qa.covetruspharmacy.io/";



//Ping Search API check authentication.
test("Return Search",async ({ request }) => {
    const response = await request.get(baseUrl+`/covetrus/search`, {
    })
    
    let responseJson = await response.json();
    for (let i = 0; i < Object.keys(responseJson).length; i++) {
        responseJson = responseJson[i]['shortcutKey'];

    }
})

//Externalize parameter data in next iteration (Sql?)
//Exception should occur
test("SAddress Book:  duplicate",async ({ request }) => {

    const createDuplicateNoteResponse = await request.post(baseUrl+`covetrus/my-account/address-book`, {
        data : {
            "FirstName": `Duplicate: "Justin"`,
            "LastName": `Duplicate: "Eubank"`,
          
        }
    })

    let duplicateResponseJson = await createDuplicateNoteResponse.json();
    let responseErrorCode = duplicateResponseJson['code'];
    expect(responseErrorCode).toContain("Address already exists");
})

test("Search: Result Count",async ({ request }) => {
    const resultCount = await request.post(baseUrl+`covetrus/`, {

        data : {
            "SearchText": `Heart `

        }
    })
    
    expect(resultCount.ok())
    //this is stupid, think of better way of accomplishing
    expect(resultCount).toEqual('12')
})


test("Search Product",async ({ request }) => {
    const searchResponse = await request.delete(baseUrl+`covetrus/my-account/address-book/delete`, {
        data : {
            "SearchQry": `Heart`
           
        }
    })
    //Stupid, figure out new way
    expect(searchResponse).toEqual("12");
    expect(searchResponse.ok()).toBeTruthy;
})

test("Product Deleted",async ({ request }) => {
    const getResponse = await request.get(baseUrl+`covetrus`, {
       
    })

    let getResponseJson = await getResponse.json();

    let isDeleted = await getResponseJson['isDeleted'];


    expect(isDeleted).toEqual(true);

})

test("Create Address",async ({ request }) => {
    
    const createResponse = await request.post(baseUrl+`covetrus/my-account/address-book`, {
    
    })

    expect(createResponse.ok).toBeTruthy;
    
    let responseJson = await createResponse.json();
    
    expect(responseJson).toBeTruthy;

})