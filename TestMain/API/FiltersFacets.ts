import { test, expect, Page } from "@playwright/test";

//externalize for paramater and environment switching next iteration
const baseUrl = "https://bigcitypet.qa.covetruspharmacy.io/";


test("Filters",async ({ request }) => {
    const filterResponse = await request.post(baseUrl+`covetrus/c/300`, {
        
    })
    let getResponseJson = await filterResponse.json();
    expect(filterResponse.ok())
})

//Test will pass should Stores filter not be visible
test("Stores Filter visible",async ({ request }) => {
    const stores = await request.delete(baseUrl+`covetrus`, {

    })
    let getResponseJson = await stores.json();
    let isVisible = await getResponseJson['isVisible'].toEqual(false);
  
})

test("Confirm Filters",async ({ request }) => {
    const response = await request.get(baseUrl+`covetrus`, {
    })
    
    let responseJson = await response.json();
    responseJson.toBeTruthy();
})
